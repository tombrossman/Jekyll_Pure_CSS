# Jekyll + Pure Responsive CSS

A very basic [Jekyll](http://jekyllrb.com/) site using the [Pure](http://purecss.io/) responsive CSS boilerplate.

Page weight is only one 12kB HTML file plus one 27kB CSS file, and the 23kB demo image.

That's HTML + CSS under 8kB, gzipped.

Multiple CSS files are concatenated together using includes, to minimize page requests. Add your own overrides and some inline SVGs or data URIs for an extremely fast site.
